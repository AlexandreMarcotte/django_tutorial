from django.shortcuts import render
from django.http import HttpResponse
from .models import Post
"""
# Dummy Data for first test

posts = [
    {'autor': 'CoreyMS',
     'title': 'Blog Post 1',
     'content': 'First post content',
     'date_posted': 'August 27, 2018'
     },
    {'autor': 'Jane Doe',
     'title': 'Blog Post 2',
     'content': 'Second post content',
     'date_posted': 'August 28, 2018'
     }
]
"""


def home(request):
    context = {
        'posts': Post.objects.all()
        # 'posts': posts      # For when we were using the dummy data
    }
    return render(request, 'blog/home.html', context)


def about(request):
    return render(request, 'blog/about.html', {'title': 'About'})
