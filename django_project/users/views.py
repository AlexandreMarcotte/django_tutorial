from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserRegisterForm


def register(request):
    if request.method == 'POST':
        # Post form
        form = UserRegisterForm(request.POST)
        # if form is valid when submited
        if form.is_valid():
            # Save the new user
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account created for {username}!')
            return redirect('blog-home')
    else:
        # Empty form
        form = UserRegisterForm()

    return render(request, 'users/register.html', {'form': form})

